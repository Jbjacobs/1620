/**
 * This class represents an abstract animal.
 * You should mark this class is abstract.
 * This class should have a private member called happiness of type int.
 * Initialize this member to 0.
 * 
 * This class should have a protected method called improveHappiness.
 * This method should return void.
 * This method should increase the value of happiness by one.
 * 
 * @author bricks
 *
 */
public abstract class AAnimal implements IAnimal{

	private int happiness = 0;
	/*
	 * I have overridden this method for you. You should not make any changes to this method.
	 */
	@Override
	public String toString() {
		return "The " + this.getClass().getName() + " has a happiness value of " + happiness;
	}
	
	protected void improveHappiness(){
		happiness++;
	}
	
	

}
