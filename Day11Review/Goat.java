/**
 * This class represents a goat.
 * 
 * Goat should inherit from AAnimal.
 * 
 * You should have the methods required to make this a concrete class.
 * 
 * Goat improves it happiness no matter what it is fed.
 * @author bricks
 *
 */
public class Goat extends AAnimal{

	@Override
	public void feed(IFood food) {
		// TODO Auto-generated method stub
		improveHappiness();
	}
	
	

	}
