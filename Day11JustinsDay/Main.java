

public class Main {

	public static void main(String[] args) {
		try{
		wakeUp();
		getReadyForSchool();
		goToClass();
		eatLunch();
		laterClass();
		goHome();
		}catch(AbductionException e){
			System.out.println("Justin heard about an abduction on campus. Maybe it was an alien...");
			System.out.println("Justin goes back to sleep");
		}
	}
	public static void wakeUp() throws AbductionException {
		System.out.println("Justin wakes up.");
		throw new AbductionException();
		
	}
	public static void getReadyForSchool(){
		System.out.println("Justin gets ready for school");
	}
	public static void goToClass()
	{
	System.out.println("Justin can't hear anything because of the fan");
	}
	public static void eatLunch(){
		System.out.println("Justin eats lunch. He has a...McRib 'sandwich'");
		
	}
	
	public static void laterClass(){
		System.out.println("Justin tries to stay awake in his later classes.");
	}
	
	public static void goHome(){
		System.out.println("Justin takes his hoverboard home. (don't take them on an airplane)");
	}
}