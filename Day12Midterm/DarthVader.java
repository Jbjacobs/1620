/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Darth Vader.
 * 
 * When defending against Michelangelo, this class should return HelperStrings.DarthVaderDefendingMichelangelo
 * 
 * When defending against Chuck Norris, this class should return HelperStrings.DarthVaderDefendingChuckNorris
 * @author bricks
 *
 */
public class DarthVader extends AContestant  {
/**
 * This is the constructor method which sets the contestants name to Darth Vader
 */
	public DarthVader() {
		super("Darth Vader");
		// TODO Auto-generated constructor stub
	}
/**
 * This method is the logic for determining the outcome when Darth Vader is on defense and returns the result as a string
 * 
 * @param contestant is the opponent the contestant is defending agains
 * @return String value of the result
 */
	@Override
	public String defendAgainst(IContestant contestant) {
	
		if (contestant.toString() == "Chuck Norris"){
			return HelperStrings.DarthVaderDefendingChuckNorris.toString();
		}
		else return HelperStrings.DarthVaderDefendingMichelangelo.toString();
	}
	
}
