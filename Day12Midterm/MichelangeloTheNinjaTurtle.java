/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Michelangelo.
 * 
 * When defending against Chuck Norris, this class should return HelperStrings.MichelangeloDefendingChuck
 * 
 * When defending against Darth Vader, this class should return HelperStrings.MichelangeloDefendingDarthVader
 * @author bricks
 *
 */
public class MichelangeloTheNinjaTurtle extends AContestant {
	/**
	 * This is the constructor method which sets the contestants name to Michelangelo
	 */
	public MichelangeloTheNinjaTurtle() {
		super("Michelangelo");
		
	}
/**
 * This method provides the logic for determining the winner when Michelangelo is on defense and returns the result as a string
 * @param contestant is the opponent the contestant is defending agains
 * @return String value of the result
 */
	@Override
	public String defendAgainst(IContestant contestant) {
		// TODO Auto-generated method stub
		if (contestant.toString() == "Chuck Norris"){
			return HelperStrings.MichelangeloDefendingChuck.toString();
		}
		else return HelperStrings.MichelangeloDefendingDarthVader.toString();
	}
		
	
}
