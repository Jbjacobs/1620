/**
 * You should change this file.
 * 
 * IContestant is an interface. It indicates what shared functionality all contestants have to have.
 * 
 * IContestant should indicate that classes that implement it should have a method called defendAgainst.
 * This method should take as a parameter a reference of type IContestant (indicating the player against which this player is defending).
 * 
 * This method should return the results of the match in the form of a String.
 * @author bricks
 *
 */

/**
 * This interface returns the results of the match in the form of a string
 * @author Jason
 *
 */
public interface IContestant {
	
	 String defendAgainst(IContestant offense);
		
}
	

