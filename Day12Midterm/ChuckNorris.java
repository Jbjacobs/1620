/**
 * You need to change this file. 
 * 
 * This class should inherit from AContestant.
 * 
 * This class should set the value of name to be Chuck Norris.
 * 
 * When defending against Michelangelo, this class should return HelperStrings.ChuckDefendingMichelangelo
 * 
 * When defending against Darth Vader, this class should return HelperStrings.ChuckDefendingDarthVader
 * @author bricks
 *
 */
public class ChuckNorris extends AContestant {
	
/**
 * This is the constructor that sets the players name to Chuck Norris
 */
	public ChuckNorris() {
		
		super("Chuck Norris");
		
	}
	/**
	 * This method is the logic for determining the outcome of the match when Chuck Norris is on defense
	 *  @param contestant is the opponent the contestant is defending agains
     * @return String value of the result
	 */
	@Override
	public String defendAgainst(IContestant contestant) {
		
		if (contestant.toString() == "Michelangelo"){
			return HelperStrings.ChuckDefendingMichelangelo.toString();
		}
		else return HelperStrings.ChuckDefendingDarthVader.toString();
		}
	
}

